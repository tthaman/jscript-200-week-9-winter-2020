// create api-key.js file with const API_KEY="your_api_key" in this same directory to use
//This app uses the NY Times API so I'm assuming the user will have a key.
//The app DOES NOT demonstrate jasmine tests or form validation

const BASE_URL = 'https://api.nytimes.com/svc/news/v3/content/';
const sourceEl  = document.getElementById('source');
let podcasts = [];

sourceEl.addEventListener('change', function (event) {
  toggleSpinner();
  getPodCasts();
  // event.preventDefault();
});

const toggleSpinner = () => {
  const spinner = document.getElementById("loader");
  const divEl = document.getElementById("pcsdiv");
  if(spinner.style.display === "none") {
    spinner.style.display = "block";
    // divEl.style.display = "none";
  } else {
    spinner.style.display = "none";
    // divEl.style.display = "block";
  }
};

//One or more Classes (must use static methods and/or prototype methods)
class Podcast {
  constructor(title, abstract, url, index) {
    this.title = title;
    this.abstract = abstract;
    this.url = url;
    this.index = index;
    this.numClicked = 0;
    this.images = [];
  }

  toString(){
    return `Title: ${this.title}
    Abstract: ${this.abstract}
    URL: ${this.url}
    Index: ${this.index}`
  }
}
function reloadPage() {
  window.location.reload();
}

const addToPreferredList = (pc) => {
  let pl = JSON.parse(localStorage.getItem("preferredList"));
  let preferredList;
  if(pl) {
    preferredList = pl;
    for (let i = 0; i < preferredList.length; i++) {
      let item = preferredList[i];
      if (item.title === pc.title) {
        return false
      }
    }
  } else {
    preferredList = [];
  }
  preferredList.push(pc);
  //Sets, updates, or changes local storage
  localStorage.setItem('preferredList', JSON.stringify(preferredList));
  renderPreferred();
  return true
};

const removeFromPreferredList = (pc) => {
  const titles = document.getElementsByTagName('h3');
  for(let i = 0; i< titles.length; i++) {
    if(titles[i].innerText === pc.title) {
      let aButton = titles[i].parentNode.previousSibling.firstChild;
      aButton.innerText = "Save";
      aButton.style="background-color: #007bff";
      break;
    }
  }
  let newList = [];
  let pl = JSON.parse(localStorage.getItem("preferredList"));
  if(pl) {
    let preferredList = pl;
    for (let i = 0; i < preferredList.length; i++) {
      let item = preferredList[i];
      if (item.title !== pc.title) {
        newList.push(item);
      }
    }
  }
  preferredList = newList;

  //Sets, updates, or changes local storage
  localStorage.setItem("preferredList", JSON.stringify(preferredList));
  renderPreferred();
};

const clearStorage = () => {
  localStorage.clear();
  return localStorage.length;
};

//One or more fetch requests to a 3rd party API
const getPodCasts = () => {
  let source = sourceEl.value;
  const url = `${BASE_URL}${source}/podcasts.json?api-key=${API_KEY}&limit=500`;
  fetch(url)
      .then(function(data) {
        return data.json();
      })
      .then(function(responseJson) {
        console.log(`response status is: ${responseJson.status}`);
        podcasts = responseJson.results;
        const mainDiv =  document.getElementById(`pcsdiv`);

        const headerRow = document.getElementById(`headerRow`);
        removeSiblings(headerRow);

        let pcdiv = document.createElement('div');
        pcdiv.className = 'row';
        pcdiv.id = 'pcdiv';
        mainDiv.appendChild(pcdiv);

        if(!podcasts > 0){
          // const headerRow = document.getElementById(`headerRow`);
          // removeSiblings(headerRow);

          // let pcdiv = document.createElement('div');
          // pcdiv.className = 'row';
          // mainDiv.appendChild(pcdiv);
          // pcdiv.id = `pcdiv`;

          let pcdiv2 = document.createElement('div');
          pcdiv2.className = 'col-md-12';
          pcdiv2.id = `pcdivTwo`;
          document.getElementById(`pcdiv`).appendChild(pcdiv2);

          let chkDiv = document.createElement('div');
          chkDiv.className = 'row';
          chkDiv.id = `chkDiv`;
          document.getElementById(`pcdivTwo`).appendChild(chkDiv);

          let chkdivl = document.createElement('div');
          chkdivl.className = 'col-md-2';
          chkdivl.id = `chkdivl`;
          document.getElementById(`chkDiv`).appendChild(chkdivl);

          let chkdivr = document.createElement('div');
          chkdivr.className = 'col-md-10';
          chkdivr.id = `chkdivr`;
          document.getElementById(`chkDiv`).appendChild(chkdivr);

          let title = document.createElement('h3');
          title.className="blinking";
          title.style = "color: red";
          title.innerHTML = 'There are no podcasts to show';
          document.getElementById(`chkdivr`).appendChild(title);

          //One or more timing functions
          setInterval(reloadPage, 5000)

        } else {
          pcdiv.color = "black";
          for(let i=0; i < podcasts.length; i++) {

            let aPodcast = new Podcast(podcasts[i].title, podcasts[i].abstract, podcasts[i].url, i);
            console.log(`podcast is ${aPodcast.toString()}`);

            pcdiv.id = `pcdiv${i}`;

            let pcdiv2 = document.createElement('div');
            pcdiv2.className = 'col-md-12';
            pcdiv2.id = `pcdivTwo${i}`;
            document.getElementById(`pcdiv${i}`).appendChild(pcdiv2);

            let chkDiv = document.createElement('div');
            chkDiv.className = 'row';
            chkDiv.id = `chkDiv${i}`;
            document.getElementById(`pcdivTwo${i}`).appendChild(chkDiv);

            let chkdivl = document.createElement('div');
            chkdivl.className = 'col-md-2';
            chkdivl.id = `chkdivl${i}`;
            document.getElementById(`chkDiv${i}`).appendChild(chkdivl);

            let chkdivr = document.createElement('div');
            chkdivr.className = 'col-md-10';
            chkdivr.id = `chkdivr${i}`;
            document.getElementById(`chkDiv${i}`).appendChild(chkdivr);

            let button = document.createElement('button');
            button.type = 'button';
            button.id = `button${i}`;
            button.className="btn btn-primary";
            if(isSaved(aPodcast.title)){
              button.innerText = 'Saved';
              button.style="background-color: green"
            } else {
              button.innerText = 'Save';
              button.style="background-color:  #007bff"
            }
            button.addEventListener('click', function (event) {
              let added = addToPreferredList(aPodcast);
              if(added) {
                if (event.target.innerText === 'Save') {
                  event.target.innerText = 'Saved';
                  event.target.style = "background-color: green";
                } else {
                  event.target.innerText = 'Save';
                  event.target.style = "background-color: #007bff";
                }
              }
              event.preventDefault();
            });
            document.getElementById(`chkdivl${i}`).appendChild(button);

            let title = document.createElement('h3');
            title.id = `title${i}`;
            title.style = "display:inline";
            document.getElementById(`chkdivr${i}`).appendChild(title);

            let goToPC = document.createElement('a');
            goToPC.id = `url${i}`;
            goToPC.href = podcasts[i].url;
            goToPC.target = "_blank";
            goToPC.innerHTML = podcasts[i].title;
            goToPC.addEventListener('click', function (event) {
              aPodcast.numClicked++
            });
            document.getElementById(`title${i}`).appendChild(goToPC);

            let abstract = document.createElement('p');
            abstract.id = `abstract${i}`;
            abstract.innerHTML = podcasts[i].abstract;
            document.getElementById(`chkdivr${i}`).appendChild(abstract);

            let lineBreak = document.createElement('br');
            document.getElementById(`pcsdiv`).appendChild(lineBreak);
          }
        }
        toggleSpinner();
      })
      .catch(error => console.log(error.message))
};

const renderPreferred = (pc) => {
    let pl = localStorage.getItem("preferredList")
    if(!pl) {
      return
    }
    let items = JSON.parse(pl);

    const mainDiv = document.getElementById(`preferred`);
    const headerRow = document.getElementById(`prefHeaderRow`);
    removeSiblings(headerRow);
    let prefDiv = document.createElement('div');
    prefDiv.className = 'row';
    mainDiv.appendChild(prefDiv);

    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      prefDiv.id = `prefdiv${i}`;

      let prefdiv2 = document.createElement('div');
      prefdiv2.className = 'col-md-12';
      prefdiv2.id = `prefdivTwo${i}`;
      document.getElementById(`prefdiv${i}`).appendChild(prefdiv2);

      let prefchkDiv = document.createElement('div');
      prefchkDiv.className = 'row';
      prefchkDiv.id = `prefchkDiv${i}`;
      document.getElementById(`prefdivTwo${i}`).appendChild(prefchkDiv);

      let prefchkdivl = document.createElement('div');
      prefchkdivl.className = 'col-md-2';
      prefchkdivl.id = `prefchkdivl${i}`;
      document.getElementById(`prefchkDiv${i}`).appendChild(prefchkdivl);

      let prefchkdivr = document.createElement('div');
      prefchkdivr.className = 'col-md-10';
      prefchkdivr.id = `prefchkdivr${i}`;
      document.getElementById(`prefchkDiv${i}`).appendChild(prefchkdivr);

      let button = document.createElement('button');
      button.type = 'button';
      button.id = `prefbutton${i}`;
      button.className="btn btn-primary";
      button.innerText = 'Remove';
      button.addEventListener('click', function (event) {
        document.getElementById(`prefchkDiv${i}`).remove();
        removeFromPreferredList(item);
        event.preventDefault();
      });
      document.getElementById(`prefchkdivl${i}`).appendChild(button);

      let title = document.createElement('h3');
      title.id = `preftitle${i}`;
      title.style = "display:inline";
      document.getElementById(`prefchkdivr${i}`).appendChild(title);

      let goToPC = document.createElement('a');
      goToPC.id = `prefurl${i}`;
      goToPC.href = item.url;
      goToPC.target = "_blank";
      goToPC.innerHTML = item.title;
      goToPC.addEventListener('click', function (event) {
        aPodcast.numClicked++
      });
      document.getElementById(`preftitle${i}`).appendChild(goToPC);

      let abstract = document.createElement('p');
      abstract.id = `prefabstract${i}`;
      abstract.innerHTML = item.abstract;
      document.getElementById(`prefchkdivr${i}`).appendChild(abstract);

      let lineBreak = document.createElement('br');
      document.getElementById(`preferred`).appendChild(lineBreak);
    }
};

const removeSiblings = (el) => {
  while (el.nextSibling)
    el.parentNode.removeChild(el.nextSibling);

};

const isSaved = (aTitle) => {
  let pl = localStorage.getItem("preferredList");
  if(pl && pl.isArray) {
    let preferredList = JSON.parse(pc);
    for (let i = 0; i < preferredList.length; i++) {
      let item = preferredList[i];
      if (item.title === aTitle) {
        return true;
      }
    }
  }
  return false;
};
